Règles stechec2 pour le projet de THJX SCIA-CSI 2016.

# Installation

## Dépendances

### Debian/Ubuntu

Installation des dépendances de stechec :

    :::bash
    apt-get install gcc python3 libzmq3-dev libzmqpp-dev libgflags-dev \
                    libgtest-dev ruby python3-yaml

Installation des dépendances des langages (prenez ce que vous voulez) :

    :::bash
    apt-get install libphp5-embed php5-dev php5  # PHP
    apt-get install python3-dev  # Python
    apt-get install ocaml  # Ocaml
    apt-get install mono-devel  # C#
    apt-get install openjdk-8-jdk  # Java
    apt-get install haskell-platform  # Haskell

### Archlinux

Installation des dépendances de stechec :

    :::bash
    pacman --needed -S gcc zeromq gtest ruby python-yaml gflags

Installation des dépendances des langages (prenez ce que vous voulez) :

    :::bash
    pacman -S --needed php php-embed  # PHP
    pacman -S --needed ocaml  # Ocaml
    pacman -S --needed mono  # C#
    pacman -S --needed jdk8-openjdk  # Java
    pacman -S --needed haskell-platform  # Haskell

## Stechec2 + THJX2016

Il faut commencer par installer
[stechec2](https://bitbucket.org/prologin/stechec2). Le README est assez clair,
mais voici à quoi ça devrait ressembler chez vous :

    :::bash
    git clone https://bitbucket.org/prologin/stechec2
    cd stechec2
    git clone https://bitbucket.org/serialk/thjx2016 games/thjx2016
    ./waf.py configure --with-games=thjx2016 --prefix=/usr
    ./waf.py build
    sudo ./waf.py install

# Génération de l'environnement

Il faut maintenant générer les fichiers de base pour commencer à coder votre
champion.

    :::bash
    stechec2-generator player thjx2016 player_env
    cd player_env/python  # Remplacer "python" par votre langage de prédilection
    $EDITOR prologin.py  # C'est le fichier à compléter
    make
    make tar # Crée une tarball à envoyer au serveur

Envoyez ensuite la tarball ``champion.tgz`` ainsi générée sur notre site.
N'oubliez pas de refaire ``make tar`` à chaque nouvelle modification de votre
code, avant de nous l'envoyer.

# Lancer un match en local

Pour lancer un match en local et ainsi le tester avant de nous l'envoyer, vous
pouvez créer un fichier ``config.yml`` qui contient des informations sur le
match, comme par exemple :

    rules: /usr/lib/libthjx2016.so
    verbose: 3
    clients:
      - ./champion.so
      - ./champion.so
    names:
      - Player 1
      - Player 2

Vous n'avez ensuite plus qu'à lancer le match en faisant :

    stechec2-run config.yml

N'hésitez pas à regarder ``stechec2-run -h`` pour d'autres options (lancer un
client avec gdb avec ``-d`` par exemple…). :-)
