///
// This file has been generated, if you wish to
// modify it in a permanent way, please refer
// to the script file : gen/generator_python.rb
//

#include "interface.hh"

static PyObject* c_module;
static PyObject* py_module;
static PyObject* champ_module;

static void _init_python();

template <typename Lang, typename Cxx>
Lang cxx2lang(Cxx in)
{
  return in.__if_that_triggers_an_error_there_is_a_problem;
}

template <>
PyObject* cxx2lang<PyObject*, int>(int in)
{
  return PyLong_FromLong(in);
}


template <>
PyObject* cxx2lang<PyObject*, std::string>(std::string in)
{
return PyUnicode_FromString (in.c_str());
}


template <>
PyObject* cxx2lang<PyObject*, bool>(bool in)
{
  return PyBool_FromLong(in);
}

template <typename Cxx>
PyObject* cxx2lang_array(const std::vector<Cxx>& in)
{
  size_t size = in.size();
  PyObject* out = PyList_New(size);

  for (unsigned int i = 0; i < size; ++i)
    PyList_SET_ITEM(out, i, (cxx2lang<PyObject*, Cxx>(in[i])));

  return out;
}

template <typename Lang, typename Cxx>
Cxx lang2cxx(Lang in)
{
  return in.__if_that_triggers_an_error_there_is_a_problem;
}

template <>
int lang2cxx<PyObject*, int>(PyObject* in)
{
  long out = PyLong_AsLong(in);
  if (out == -1)
    if (PyErr_Occurred())
    {
      throw 42;
    }

  return out;
}

template <>
bool lang2cxx<PyObject*, bool>(PyObject* in)
{
  return (bool)lang2cxx<PyObject*, int>(in);
}

template <>
std::string lang2cxx<PyObject*, std::string>(PyObject* in)
{
  char * out = PyUnicode_AsUTF8(in);
  if (PyErr_Occurred())
    {
      throw 42;
    }
  return out;
}

template <typename Cxx>
std::vector<Cxx> lang2cxx_array(PyObject* in)
{
  if (!PyList_Check(in))
  {
    PyErr_SetString(PyExc_TypeError, "a list is required");
    throw 42;
  }

  std::vector<Cxx> out;
  unsigned int size = PyList_Size(in);

  for (unsigned int i = 0; i < size; ++i)
    out.push_back(lang2cxx<PyObject*, Cxx>(PyList_GET_ITEM(in, i)));

  return out;
}
///
// Errors returned by the actions
//
template <>
PyObject* cxx2lang<PyObject*, action_error>(action_error in)
{
  PyObject* name = PyUnicode_FromString("action_error");
  PyObject* enm = PyObject_GetAttr(py_module, name);
  if (enm == NULL) throw 42;
  PyObject* arglist = Py_BuildValue("(i)", (int) in);
  PyObject* ret = PyObject_CallObject(enm, arglist);
  Py_DECREF(name);
  Py_DECREF(arglist);
  Py_DECREF(enm);
  return ret;
}

template <>
action_error lang2cxx<PyObject*, action_error>(PyObject* in)
{
  return (action_error)lang2cxx<PyObject*, int>(in);
}

///
// Type of elements
//
template <>
PyObject* cxx2lang<PyObject*, element>(element in)
{
  PyObject* name = PyUnicode_FromString("element");
  PyObject* enm = PyObject_GetAttr(py_module, name);
  if (enm == NULL) throw 42;
  PyObject* arglist = Py_BuildValue("(i)", (int) in);
  PyObject* ret = PyObject_CallObject(enm, arglist);
  Py_DECREF(name);
  Py_DECREF(arglist);
  Py_DECREF(enm);
  return ret;
}

template <>
element lang2cxx<PyObject*, element>(PyObject* in)
{
  return (element)lang2cxx<PyObject*, int>(in);
}

///
// Bid resources in the elements auction.
//
static PyObject* p_bid(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, action_error>(api_bid(lang2cxx<PyObject*, int>(a0)));
  } catch (...) { return NULL; }
}

///
// Outbid the current best bid.
//
static PyObject* p_outbid(PyObject* self, PyObject* args)
{
  (void)self;
  if (!PyArg_ParseTuple(args, "")) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, action_error>(api_outbid());
  } catch (...) { return NULL; }
}

///
// Play an element against the other player.
//
static PyObject* p_fight(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, action_error>(api_fight(lang2cxx<PyObject*, element>(a0)));
  } catch (...) { return NULL; }
}

///
// Returns your player identifier (not always in {0, 1}).
//
static PyObject* p_me(PyObject* self, PyObject* args)
{
  (void)self;
  if (!PyArg_ParseTuple(args, "")) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_me());
  } catch (...) { return NULL; }
}

///
// Returns your opponent identifier (not always in {0, 1}).
//
static PyObject* p_opponent(PyObject* self, PyObject* args)
{
  (void)self;
  if (!PyArg_ParseTuple(args, "")) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_opponent());
  } catch (...) { return NULL; }
}

///
// The current score of the specified player.
//
static PyObject* p_score(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_score(lang2cxx<PyObject*, int>(a0)));
  } catch (...) { return NULL; }
}

///
// The set of elements owned by a specific player.
//
static PyObject* p_elements_owned(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
return cxx2lang_array(api_elements_owned(lang2cxx<PyObject*, int>(a0)));
  } catch (...) { return NULL; }
}

///
// The quantity of an element owned by a specific player.
//
static PyObject* p_nb_element_owned(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
PyObject* a1;
  if (!PyArg_ParseTuple(args, "OO", &a0, &a1)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_nb_element_owned(lang2cxx<PyObject*, int>(a0), lang2cxx<PyObject*, element>(a1)));
  } catch (...) { return NULL; }
}

///
// The quantity of all the elements owned by a specific player.
//
static PyObject* p_nb_elements_owned(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_nb_elements_owned(lang2cxx<PyObject*, int>(a0)));
  } catch (...) { return NULL; }
}

///
// The gain of an element played against another (returns a value from the gain matrix).
//
static PyObject* p_resources_gain(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
PyObject* a1;
  if (!PyArg_ParseTuple(args, "OO", &a0, &a1)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_resources_gain(lang2cxx<PyObject*, element>(a0), lang2cxx<PyObject*, element>(a1)));
  } catch (...) { return NULL; }
}

///
// The amount of resources owned by a player.
//
static PyObject* p_resources_owned(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_resources_owned(lang2cxx<PyObject*, int>(a0)));
  } catch (...) { return NULL; }
}

///
// The last element that was played during a fight by the specified player. Returns ``elt_invalid`` when called during the first turn.
//
static PyObject* p_last_element_played(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, element>(api_last_element_played(lang2cxx<PyObject*, int>(a0)));
  } catch (...) { return NULL; }
}

///
// The element currently sold in auction. Returns ``elt_invalid`` if not in auction phase.
//
static PyObject* p_element_sold(PyObject* self, PyObject* args)
{
  (void)self;
  if (!PyArg_ParseTuple(args, "")) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, element>(api_element_sold());
  } catch (...) { return NULL; }
}

///
// The initial bid of a specific player. Returns -1 if not in auction phase.
//
static PyObject* p_player_initial_bid(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_player_initial_bid(lang2cxx<PyObject*, int>(a0)));
  } catch (...) { return NULL; }
}

///
// The current bid of a specific player. Returns -1 if not in auction phase.
//
static PyObject* p_player_bid(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_player_bid(lang2cxx<PyObject*, int>(a0)));
  } catch (...) { return NULL; }
}

///
// The total cost of outbidding ($m_k$). Returns -1 if not in outbidding phase.
//
static PyObject* p_outbid_cost(PyObject* self, PyObject* args)
{
  (void)self;
  if (!PyArg_ParseTuple(args, "")) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_outbid_cost());
  } catch (...) { return NULL; }
}

///
// The difference between the initial bids. Returns -1 if not in outbidding phase.
//
static PyObject* p_bid_difference(PyObject* self, PyObject* args)
{
  (void)self;
  if (!PyArg_ParseTuple(args, "")) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_bid_difference());
  } catch (...) { return NULL; }
}

///
// Returns ``true`` if the last auction was canceled and ``false`` in all the other cases.
//
static PyObject* p_auction_canceled(PyObject* self, PyObject* args)
{
  (void)self;
  if (!PyArg_ParseTuple(args, "")) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, bool>(api_auction_canceled());
  } catch (...) { return NULL; }
}

///
// The identifier of the last player who won the auction. Returns -1 if the auction was canceled.
//
static PyObject* p_auction_winner(PyObject* self, PyObject* args)
{
  (void)self;
  if (!PyArg_ParseTuple(args, "")) {
    return NULL;
  }
    try {
return cxx2lang<PyObject*, int>(api_auction_winner());
  } catch (...) { return NULL; }
}

///
// Affiche le contenu d'une valeur de type action_error
//
static PyObject* p_afficher_action_error(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
api_afficher_action_error(lang2cxx<PyObject*, action_error>(a0));
  Py_INCREF(Py_None);
  return Py_None;
  } catch (...) { return NULL; }
}

///
// Affiche le contenu d'une valeur de type element
//
static PyObject* p_afficher_element(PyObject* self, PyObject* args)
{
  (void)self;
PyObject* a0;
  if (!PyArg_ParseTuple(args, "O", &a0)) {
    return NULL;
  }
    try {
api_afficher_element(lang2cxx<PyObject*, element>(a0));
  Py_INCREF(Py_None);
  return Py_None;
  } catch (...) { return NULL; }
}


/*
** Api functions to register.
*/
static PyMethodDef api_callback[] = {
  {"bid", p_bid, METH_VARARGS, "bid"},  {"outbid", p_outbid, METH_VARARGS, "outbid"},  {"fight", p_fight, METH_VARARGS, "fight"},  {"me", p_me, METH_VARARGS, "me"},  {"opponent", p_opponent, METH_VARARGS, "opponent"},  {"score", p_score, METH_VARARGS, "score"},  {"elements_owned", p_elements_owned, METH_VARARGS, "elements_owned"},  {"nb_element_owned", p_nb_element_owned, METH_VARARGS, "nb_element_owned"},  {"nb_elements_owned", p_nb_elements_owned, METH_VARARGS, "nb_elements_owned"},  {"resources_gain", p_resources_gain, METH_VARARGS, "resources_gain"},  {"resources_owned", p_resources_owned, METH_VARARGS, "resources_owned"},  {"last_element_played", p_last_element_played, METH_VARARGS, "last_element_played"},  {"element_sold", p_element_sold, METH_VARARGS, "element_sold"},  {"player_initial_bid", p_player_initial_bid, METH_VARARGS, "player_initial_bid"},  {"player_bid", p_player_bid, METH_VARARGS, "player_bid"},  {"outbid_cost", p_outbid_cost, METH_VARARGS, "outbid_cost"},  {"bid_difference", p_bid_difference, METH_VARARGS, "bid_difference"},  {"auction_canceled", p_auction_canceled, METH_VARARGS, "auction_canceled"},  {"auction_winner", p_auction_winner, METH_VARARGS, "auction_winner"},  {"afficher_action_error", p_afficher_action_error, METH_VARARGS, "afficher_action_error"},  {"afficher_element", p_afficher_element, METH_VARARGS, "afficher_element"},  {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC PyInit__api()
{
  static struct PyModuleDef apimoduledef = {
      PyModuleDef_HEAD_INIT,
      "_api",
      "API module",
      -1,
      api_callback,
      NULL,
      NULL,
      NULL,
      NULL,
  };
  return PyModule_Create(&apimoduledef);
}


/*
** Load a Python module
*/

static PyObject* _import_module(const char* m)
{
  PyObject* name = PyUnicode_FromString(m);
  PyObject* module = PyImport_Import(name);
  Py_DECREF(name);
  if (module == NULL)
    if (PyErr_Occurred())
    {
      PyErr_Print();
      abort();
    }
  return module;
}

/*
** Inititialize python, register API functions,
** and load .py file
*/
static void _init_python()
{
  static wchar_t empty_string[] = L"";
  static wchar_t *argv[] = { (wchar_t *) &empty_string, NULL };

  const char* champion_path;

  champion_path = getenv("CHAMPION_PATH");
  if (champion_path == NULL)
    champion_path = ".";

  setenv("PYTHONPATH", champion_path, 1);

  static wchar_t program_name[] = L"stechec";
  Py_SetProgramName(program_name);

  PyImport_AppendInittab("_api", PyInit__api);
  Py_Initialize();
  PySys_SetArgvEx(1, argv, 0);

  champ_module = _import_module("prologin");
  py_module = _import_module("api");
}

/*
** Run a python function.
*/
static PyObject* _call_python_function(const char* name)
{
  static bool initialized = false;

  if (!initialized)
  {
    initialized = true;
    _init_python();
  }

  PyObject *arglist, *func;
  PyObject *result = NULL;

  func = PyObject_GetAttrString(champ_module, (char*)name);
  if (func && PyCallable_Check(func))
  {
    arglist = Py_BuildValue("()");
    result = PyEval_CallObject(func, arglist);
    Py_XDECREF(arglist);
    Py_DECREF(func);
  }
  if (result == NULL && PyErr_Occurred())
    PyErr_Print();

  return result;
}

/*
** Functions called from stechec to C.
*/
extern "C" void game_init()
{
  PyObject* _retval = _call_python_function("game_init");
  if (!_retval && PyErr_Occurred()) { PyErr_Print(); abort(); }
  try {
  Py_XDECREF(_retval);
  } catch (...) { PyErr_Print(); abort(); }
}

extern "C" void bidding_phase()
{
  PyObject* _retval = _call_python_function("bidding_phase");
  if (!_retval && PyErr_Occurred()) { PyErr_Print(); abort(); }
  try {
  Py_XDECREF(_retval);
  } catch (...) { PyErr_Print(); abort(); }
}

extern "C" void outbidding_phase()
{
  PyObject* _retval = _call_python_function("outbidding_phase");
  if (!_retval && PyErr_Occurred()) { PyErr_Print(); abort(); }
  try {
  Py_XDECREF(_retval);
  } catch (...) { PyErr_Print(); abort(); }
}

extern "C" void fighting_phase()
{
  PyObject* _retval = _call_python_function("fighting_phase");
  if (!_retval && PyErr_Occurred()) { PyErr_Print(); abort(); }
  try {
  Py_XDECREF(_retval);
  } catch (...) { PyErr_Print(); abort(); }
}

extern "C" void game_end()
{
  PyObject* _retval = _call_python_function("game_end");
  if (!_retval && PyErr_Occurred()) { PyErr_Print(); abort(); }
  try {
  Py_XDECREF(_retval);
  } catch (...) { PyErr_Print(); abort(); }
}

