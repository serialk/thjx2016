# -*- coding: utf-8 -*-
# This file has been generated, if you wish to
# modify it in a permanent way, please refer
# to the script file : gen/generator_python.rb

from api import *

import random

max_cost = 0

# Function called at the start of the game. You can use it to initialize your data structures.
def game_init():
    print('GAME INIT LOL')
    pass # Place ton code ici

# Function called during the bidding phase. If you never call the ``bid`` function, you will bid zero resources.
def bidding_phase():
    print('BIDDING LOL')
    pass

# Function called during the outbidding phase.
def outbidding_phase():
    print('OUTBIDDING LOL')
    pass

# Function called during the fighting phase. If you never call the ``fight`` function, you will play the first element you own, ordered by F > E > T > A > L.
def fighting_phase():
    print('FIGHTING LOL')
    pass

# Function called at the end of the game. You can use it to free your data structures.
def game_end():
    print('END LOL')
    pass # Place ton code ici
