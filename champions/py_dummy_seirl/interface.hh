///
// This file has been generated, if you wish to
// modify it in a permanent way, please refer
// to the script file : gen/generator_python.rb
//

#ifndef INTERFACE_HH_
# define INTERFACE_HH_

# include <Python.h>
# include <vector>
# include <string>

///
// Errors returned by the actions
//
typedef enum action_error {
  OK, /* <- The action was performed successfully. */
  INVALID_ARGUMENT, /* <- Invalid argument received. */
  WRONG_PHASE, /* <- This action cannot be performed during this phase. */
  LACK_ELEMENT, /* <- You do not own this element type. */
  LACK_RESOURCES, /* <- You need more resources to perform this action. */
  ALREADY_CALLED, /* <- You cannot call this action multiple times. */
} action_error;


///
// Type of elements
//
typedef enum element {
  ELT_FEU, /* <- Feu */
  ELT_EAU, /* <- Eau */
  ELT_TERRE, /* <- Terre */
  ELT_AIR, /* <- Air */
  ELT_LUMIERE, /* <- Lumière */
  ELT_INVALID, /* <- Invalid element */
} element;



extern "C" {

///
// Bid resources in the elements auction.
//
action_error api_bid(int amount);

///
// Outbid the current best bid.
//
action_error api_outbid();

///
// Play an element against the other player.
//
action_error api_fight(element elt);

///
// Returns your player identifier (not always in {0, 1}).
//
int api_me();

///
// Returns your opponent identifier (not always in {0, 1}).
//
int api_opponent();

///
// The current score of the specified player.
//
int api_score(int player);

///
// The set of elements owned by a specific player.
//
std::vector<element> api_elements_owned(int player);

///
// The quantity of an element owned by a specific player.
//
int api_nb_element_owned(int player, element elt);

///
// The quantity of all the elements owned by a specific player.
//
int api_nb_elements_owned(int player);

///
// The gain of an element played against another (returns a value from the gain matrix).
//
int api_resources_gain(element yours, element theirs);

///
// The amount of resources owned by a player.
//
int api_resources_owned(int player);

///
// The last element that was played during a fight by the specified player. Returns ``elt_invalid`` when called during the first turn.
//
element api_last_element_played(int player);

///
// The element currently sold in auction. Returns ``elt_invalid`` if not in auction phase.
//
element api_element_sold();

///
// The initial bid of a specific player. Returns -1 if not in auction phase.
//
int api_player_initial_bid(int player);

///
// The current bid of a specific player. Returns -1 if not in auction phase.
//
int api_player_bid(int player);

///
// The total cost of outbidding ($m_k$). Returns -1 if not in outbidding phase.
//
int api_outbid_cost();

///
// The difference between the initial bids. Returns -1 if not in outbidding phase.
//
int api_bid_difference();

///
// Returns ``true`` if the last auction was canceled and ``false`` in all the other cases.
//
bool api_auction_canceled();

///
// The identifier of the last player who won the auction. Returns -1 if the auction was canceled.
//
int api_auction_winner();

///
// Affiche le contenu d'une valeur de type action_error
//
void api_afficher_action_error(action_error v);

///
// Affiche le contenu d'une valeur de type element
//
void api_afficher_element(element v);

}

#endif // !INTERFACE_HH_
