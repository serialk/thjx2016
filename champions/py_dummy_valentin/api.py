# -*- coding: iso-8859-1 -*-
from _api import *

# Initial resources of each player at the start of the game.
INITIAL_RESOURCES = 1000

# Resources needed to win the game.
WIN_RESOURCES = 20000

# The number of elements.
NB_ELEMENTS = 5


from enum import IntEnum

# Errors returned by the actions
class action_error(IntEnum):
    OK = 0  # <- The action was performed successfully.
    INVALID_ARGUMENT = 1  # <- Invalid argument received.
    WRONG_PHASE = 2  # <- This action cannot be performed during this phase.
    LACK_ELEMENT = 3  # <- You do not own this element type.
    LACK_RESOURCES = 4  # <- You need more resources to perform this action.
    ALREADY_CALLED = 5  # <- You cannot call this action multiple times.


# Type of elements
class element(IntEnum):
    ELT_FEU = 0  # <- Feu
    ELT_EAU = 1  # <- Eau
    ELT_TERRE = 2  # <- Terre
    ELT_AIR = 3  # <- Air
    ELT_LUMIERE = 4  # <- Lumière
    ELT_INVALID = 5  # <- Invalid element


from collections import namedtuple

