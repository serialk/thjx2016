#! /usr/bin/env python2

import glob
import os.path


def options(opt):
    pass

def configure(cfg):
    pass

def build(bld):
    bld.shlib(
        source = '''
            src/action_bid.cc
            src/action_outbid.cc
            src/action_fight.cc
            src/api.cc
            src/entry.cc
            src/game_state.cc
            src/dumper.cc
            src/interface.cc
            src/rules.cc
        ''',
        defines = ['MODULE_COLOR=ANSI_COL_BROWN', 'MODULE_NAME="rules"'],
        target = 'thjx2016',
        use = ['stechec2'],
    )

    abs_pattern = os.path.join(bld.path.abspath(), 'src/tests/test-*.cc')
    for test_src in glob.glob(abs_pattern):

        test_name = os.path.split(test_src)[-1]
        test_name = test_name[5:-3]

        # Waf requires a relative path for the source
        src_relpath = os.path.relpath(test_src, bld.path.abspath())

        bld.program(
            features = 'gtest',
            source = src_relpath,
            target = 'thjx2016-test-{}'.format(test_name),
            use = ['thjx2016', 'stechec2-utils'],
            includes = ['.'],
            defines = ['MODULE_COLOR=ANSI_COL_PURPLE',
                       'MODULE_NAME="thjx2016"'],
        )

    bld.install_files('${PREFIX}/share/stechec2/thjx2016', [
        'thjx2016.yml',
    ])
