#!/usr/bin/python

import django
import os
import sys
import time

from django.contrib.auth.models import User
from prologin.concours.stechec.models import Tournament, Match, MatchPlayer, Champion, TournamentPlayer, Map

bots = {
    206: 'bear',
    208: 'bull',
    209: 'lighter',
    214: 'abitbol',
    216: 'mimic',
}

def get_champions():
    chs = []
    for u in User.objects.all():
        ch = Champion.objects.filter(author=u, author__is_staff=False,
                                     deleted=False).order_by('-id')
        if len(ch) > 0:
            chs.append(ch[0])
    for bot in bots.keys():
        chs.append(Champion.objects.get(pk=bot))
    return chs
