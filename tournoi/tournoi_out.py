#!/usr/bin/python

import django
import os
import sys
import time
from collections import defaultdict

if len(sys.argv) < 2:
    print('Usage: {} tournament_id'.format(sys.argv[0]))
    sys.exit(1)

os.environ['DJANGO_SETTINGS_MODULE'] = 'prologin.concours.settings'
django.setup()

from django.contrib.auth.models import User
from prologin.concours.stechec.models import Tournament, Match, MatchPlayer, Champion, TournamentPlayer, Map

from tournoi_common import bots, get_champions

chs = get_champions()

tournoi = Tournament.objects.get(id=int(sys.argv[1]))
matches = Match.objects.filter(tournament=tournoi)

score = defaultdict(int)
indice = defaultdict(int)
matrix = defaultdict(list)
for m in matches:
    c1, c2 = tuple(MatchPlayer.objects.filter(match=m))
    matrix[(c1.champion.id, c2.champion.id)].append(c1.score)
    matrix[(c2.champion.id, c1.champion.id)].append(c2.score)

    # Indices
    indice[c1.champion.id] += c1.score
    indice[c2.champion.id] += c2.score

    # Victories
    if c1.score > c2.score:
        score[c1.champion.id] += 2
    elif c2.score > c1.score:
        score[c2.champion.id] += 2
    else:
        score[c1.champion.id] += 1
        score[c2.champion.id] += 1

print('<!DOCTYPE html><head><title>THJX 2016 : Résultats</title></head><body>')

print('''<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse
}

th, td {
    padding-left: 10px;
    padding-right: 10px;
}
</style>''')
print('<h1>Le classement</h1>')

print('<table>')
print('<thead><td>#</td><td>Score</td><td>Victoires</td><td>Nom</td><td>Équipe</td><td>Auteurs</td></thead>')
l = chs[:]
l.sort(key=lambda x: -indice.get(x.pk, 0))
for i, c in enumerate(l, 1):
    print('<tr>')
    print('<td>{}</td>'.format(i))
    print('<td>{}</td>'.format(indice.get(c.pk, 0)))
    print('<td>{}</td>'.format(score.get(c.pk, 0) // 2))
    print('<td><strong>{}</strong></td>'.format(c.name))
    if c.pk in bots:
        print(' <td colspan="2"><strong style="color: cyan">BOT</strong></td>')
    else:
        print('<td>{}</td><td>{}</td>'.format(c.author.username,
            ' et '.join(list(filter(bool, (c.author.first_name,
                c.author.last_name))))))
    print('</tr>')
print('</table>')

#print('Saving...')
#for (score, indice, id) in l:
#    ch = Champion.objects.get(pk=id)
#    p = TournamentPlayer(
#        champion = ch,
#        tournament = tournoi,
#        score = score
#    )
#    p.save()


print('<h1>Les scores</h1>')

print('<table>')
print('<thead>')
print('<td>VS</td>')
for i, c in enumerate(l, 1):
    print('<td>{}</td>'.format(i))
print('<td>Total</td>')
print('</thead>')

for i, c1 in enumerate(l, 1):
    color = 'cyan' if c1.pk in bots else 'black'
    print('<tr><td><strong style="color: {}">{}</strong>'.format(color, i))
    for j, c2 in enumerate(l, 1):
        if c1.pk == c2.pk:
            print('<td>X</td>')
        else:
            color = 'red' if sum(matrix[c1.pk, c2.pk]) < sum(matrix[c2.pk, c1.pk]) else 'green'
            print('<td style="color: {}">{}</td>'.format(color, sum(matrix[c1.pk, c2.pk])))
    print('<td><strong>{}</strong></td>'.format(indice.get(c1.pk, 0)))
    print('</tr>')
print('</table>')


print('<h1>Les bots vaincus</h1>')

print('<table>')
print('<thead>')
print('<td>VS</td>')
print('<td>Auteurs</td>')
for bot in [c for c in l if c.pk in bots]:
    print('<td>{}</td>'.format(bot.name))
print('<td>Total vaincus</td>')
print('</thead>')


for i, c1 in enumerate(l, 1):
    if c1.pk in bots:
        continue
    print('<tr><td><strong>{}</strong></td>'.format(i))
    print('<td>{}</td>'.format(
        ' et '.join(list(filter(bool, (c1.author.first_name,
            c1.author.last_name))))))
    tot = 0
    for j, c2 in enumerate(l, 1):
        if c2.pk not in bots:
            continue
        win = False if sum(matrix[c1.pk, c2.pk]) < sum(matrix[c2.pk, c1.pk]) else True
        color = 'green' if win else 'red'
        msg = 'GAGNÉ' if win else 'PERDU'
        tot += win
        print('<td style="color: {}">{}</td>'.format(color, msg))
    print('<td><strong>{}</strong></td>'.format(tot))
    print('</tr>')
print('</table>')


print('''<h2>Les stratégies des bots</h2>
<ul>
<li><strong>Bull</strong> : Achète toujours tant qu'il peut. Enchérit
en conséquence. Première enchère arbitraire fixe (mais pas trop haute).
Joue la dernière stratégie qu'il vient d'acheter.
Sinon, celle qu'il a le plus en réserve (aléa en cas d'égalité).
</li>

<li><strong>Bear</strong> :
N'achète jamais rien.
Joue la dernière stratégie qu'il vient d'acheter.
Sinon, celle qu'il a le plus en réserve (aléa en cas d'égalité).
</li>

<li><strong>Lighter</strong> :
N'achète QUE L et met le paquet de points pour y arriver.
Ne joue QUE L.
</li>

<li><strong>Mimic</strong> :
Premier tour d'enchère : rien. Ensuite, mise autant que la dernière mise de
l'adversaire puis enchérit autant de fois que l'adversaire l'a fait aux
enchères précédentes. Tout ceci dans la limite de ses possibilités...
</li>

<li><strong>Abitbol (bot de l'équipe serveur)</strong> :
Pour la phase d'enchères, on mise une fois sur deux 10% de nos ressources, une
fois sur deux 50%. On ne surenchérit jamais. Pour la phase de combat, si nous
possédons tout deux plus qu'une lumière, on joue lumière. Sinon, on joue
l'élément parmi ceux que nous possédons qui bat l'élément que possède l'autre
joueur en plus grande quantité.
</li>

</ul>''')


print('<h1>Les détails des {} matchs</h1>'.format(matches.count()))

for i, c1 in enumerate(l, 1):
    print('<h2>{}. {} (de {})</h2>'.format(i, c1.name, c1.author.username))

    print('<table>')
    print('<thead>')
    print('<td>VS</td>')
    for i in range(10):
        print('<td>Match {}</td>'.format(i + 1))
    print('<td>Total</td>')
    print('</thead>')

    for i, c2 in enumerate(l, 1):
        color = 'cyan' if c2.pk in bots else 'black'
        print('<tr><td><strong style="color: {}">{}</strong>'.format(color, i))

        for j in range(10):
            if c1.pk == c2.pk:
                print('<td>X</td>')
            else:
                color = 'red' if matrix[c1.pk, c2.pk][j] < matrix[c2.pk, c1.pk][j] else 'green'
                print('<td style="color: {}">{}</td>'.format(color, matrix[c1.pk, c2.pk][j]))
        print('<td><strong>{}</strong></td>'.format(sum(matrix[c1.pk, c2.pk])))
        print('</tr>')
    print('</table>')

print('</body></html>')
