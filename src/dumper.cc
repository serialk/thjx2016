#include <iostream>

#include "dumper.hh"
#include "constant.hh"
#include "game_state.hh"

/* Put some binary content as a JSON string in the output stream. */
static void dump_binary(
    std::ostream& ss, const uint8_t bytes[], unsigned size
)
{
    const char* hextable = "0123456789ABCDEF";

    ss << "\"";
    for (unsigned i = 0; i < size; ++i)
    {
        if (bytes[i] == '"')
            ss << "\\\"";
        else if (bytes[i] == '\\')
            ss << "\\\\";
        else if (0x20 <= bytes[i] && bytes[i] <= 0x7e)
            ss << (char) bytes[i];
        else
            ss << "\\u00"
               << hextable[bytes[i] >> 4]
               << hextable[bytes[i] & 0x0f];
    }
    ss << "\"";
}

/* Dump everything that describe players: score and position on the map.  */

static void dump_players(std::ostream& ss, const GameState& st)
{
    auto& players = st.get_players()->players;
    bool is_first = true;

    ss << "{";
    for (unsigned i = 0; i < players.size(); ++i)
    {
        const int id = players[i]->id;

        if (players[i]->type != rules::PLAYER)
            continue;

        if (!is_first)
            ss << ", ";
        is_first = false;
        ss << "\"" << id << "\": {"
           << "\"name\": ";
        dump_binary(
            ss,
            reinterpret_cast<const uint8_t *>(players[i]->name.c_str()),
            players[i]->name.size()
        );
        ss << ", "
           << "\"resources\": " << st.get_resources(id) << ",";

        ss << "\"elements\": {";
        for (unsigned e = 0; e < NB_ELEMENTS; e++)
        {
            if (e != 0)
                ss << ", ";
            ss << "\"" << e << "\": "
               << st.get_nb_element(id, static_cast<element>(e));
        }
        ss << "}, ";

        ss << "\"initial_bid\": " << st.get_initial_bid(id) << ", "
           << "\"current_bid\": " << st.get_current_bid(id) << ", "
           << "\"played_element\": "
           << st.get_played_element(id) << ", "
           << "\"score\": " << players[i]->score
           << "}";
    }
    ss << "}";
}

void dump_game_state(std::ostream& out, const GameState& st)
{
    out << "{";

    out << "\"phase\": ";
    switch (st.get_phase())
    {
        case PHASE_BIDDING:
            out << "\"PHASE_BIDDING\"";
            break;
        case PHASE_OUTBIDDING:
            out << "\"PHASE_OUTBIDDING\"";
            break;
        case PHASE_FIGHTING:
            out << "\"PHASE_FIGHTING\"";
            break;
    }
    out << ", ";

    out << "\"element_sold\": " << st.get_elt_sold() << ", ";
    out << "\"auction_canceled\": " << st.get_auction_canceled() << ", ";
    out << "\"last_auction_winner\": " << st.get_last_auction_winner() << ", ";

    out << "\"players\": ";
    dump_players(out, st);

    out << "}\n";
}

