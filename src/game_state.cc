#include "game_state.hh"
#include "table_moves.hh"

GameState::GameState(rules::Players_sptr players)
    : rules::GameState()
    , players_(players)
    , phase_(PHASE_BIDDING)
    , elt_sold_(ELT_INVALID)
    , auction_canceled_(false)
    , finished_(false)
    , last_auction_winner_(-1)
{
    unsigned pi = 0;
    for (auto& p : players_->players)
        if (p->type == rules::PLAYER)
        {
            p->score = 0;
            player_info_[p->id] = { INITIAL_RESOURCES, {{1, 1, 1, 1, 1}}, 0, 0,
                                    ELT_INVALID, ELT_INVALID, &p->score };
            p_[pi++] = p->id;
        }
}

rules::GameState* GameState::copy() const
{
    return new GameState(*this);
}

element GameState::auction_random_element()
{
    std::array<unsigned, NB_ELEMENTS> weights{{}};
    for (unsigned i = 0; i < NB_ELEMENTS; ++i)
        weights[i] = get_nb_element(p_[0], static_cast<element>(i)) +
                     get_nb_element(p_[1], static_cast<element>(i));

    unsigned card = std::accumulate(std::begin(weights), std::end(weights), 0);
    assert(card > 0); // The game should already be over.

    std::uniform_int_distribution<int> distr(0, card - 1);
    unsigned rnd = distr(gen_);

    for (unsigned i = 0; i < NB_ELEMENTS; ++i)
    {
        if (rnd < weights[i])
            return static_cast<element>(i);
        rnd -= weights[i];
    }

    // Should never happen
    return ELT_INVALID;
}

void GameState::init_bidding()
{
    elt_sold_ = auction_random_element();
    auction_canceled_ = false;

    for (auto& pi : player_info_)
    {
        pi.second.initial_bid = 0;
        pi.second.current_bid = 0;
    }
}

int GameState::resolve_bidding()
{
    if (get_initial_bid(p_[0]) == get_initial_bid(p_[1]))
    {
        auction_canceled_ = true;
        last_auction_winner_ = -1;
        set_phase(PHASE_FIGHTING);
        return -1;
    }
    set_phase(PHASE_OUTBIDDING);
    return initial_bid_winner();
}

bool GameState::resolve_outbidding_turn(unsigned last_player_id)
{
    // It is the "end" of the outbidding phase only if the last player did not
    // outbid, which means the current bid of the last player is less than the
    // one of his opponent.
    if (get_current_bid(last_player_id) <
        get_current_bid(opponent(last_player_id)))
    {
        unsigned winner = outbid_winner();
        auto& pi = player_info_.at(winner);
        pi.resources -= bidding_final_cost();
        pi.elements[elt_sold_]++;
        last_auction_winner_ = winner;
        return true;
    }
    return false;
}

void GameState::resolve_outbidding()
{
    set_phase(PHASE_FIGHTING);
}

void GameState::init_fight()
{
    for (auto& pi : player_info_)
        pi.second.played_element = ELT_INVALID;
}

void GameState::auto_fight(unsigned player_id)
{
    if (get_played_element(player_id) == ELT_INVALID)
        for (unsigned e = 0; e < NB_ELEMENTS; ++e)
        {
            element elt = static_cast<element>(e);
            if (get_nb_element(player_id, elt) > 0)
            {
                set_played_element(player_id, elt);
                return;
            }
        }
}

void GameState::resolve_fight()
{
    for (auto& pi : player_info_)
    {
        element my = pi.second.played_element;
        element theirs = get_played_element(opponent(pi.first));
        int gain = table_moves[my][theirs];
        pi.second.resources += gain;
        if (gain <= 0)
            pi.second.elements[my]--;

        if (pi.second.elements[ELT_LUMIERE] > 0)
            *pi.second.score += pi.second.resources;

        pi.second.last_played_element = my;
    }

    for (auto& pi : player_info_)
    {
        pi.second.played_element = ELT_INVALID;

        if (pi.second.elements[ELT_LUMIERE] == 0 ||
            pi.second.resources > WIN_RESOURCES)
            finished_ = true;
    }
    set_phase(PHASE_BIDDING);
}

bool GameState::is_finished() const
{
    return finished_;
}
