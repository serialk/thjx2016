#ifndef ACTION_BID_HH
#define ACTION_BID_HH

#include <rules/action.hh>

#include "actions.hh"
#include "game_state.hh"
#include "constant.hh"

class ActionBid : public rules::Action<GameState>
{
public:
    ActionBid(int amount, int player_id) : amount_(amount), player_id_(player_id) {}
    ActionBid() {} // for register_action()

    virtual int check(const GameState* st) const;
    virtual void apply_on(GameState* st) const;

    virtual void handle_buffer(utils::Buffer& buf)
    {
        buf.handle(amount_);
        buf.handle(player_id_);
    }

    uint32_t player_id() const { return player_id_; };
    uint32_t id() const { return ID_ACTION_BID; }

private:
    int amount_;
    int player_id_;
};

#endif // !ACTION_BID_HH
