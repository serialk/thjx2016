#ifndef ACTIONS_HH
#define ACTIONS_HH

enum action_id {
    ID_ACTION_BID,
    ID_ACTION_OUTBID,
    ID_ACTION_FIGHT
};

#include "action_bid.hh"
#include "action_outbid.hh"
#include "action_fight.hh"

#endif // !ACTIONS_HH
