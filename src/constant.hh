/*
** Stechec project is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** The complete GNU General Public Licence Notice can be found as the
** `NOTICE' file in the root directory.
**
** Copyright (C) 2015 Prologin
*/

#ifndef CONSTANT_HH_
# define CONSTANT_HH_

///
// Initial resources of each player at the start of the game.
//
# define INITIAL_RESOURCES         1000

///
// Resources needed to win the game.
//
# define WIN_RESOURCES             20000

///
// The number of elements.
//
# define NB_ELEMENTS               5

///
// Errors returned by the actions
//
typedef enum action_error {
  OK, /* <- The action was performed successfully. */
  INVALID_ARGUMENT, /* <- Invalid argument received. */
  WRONG_PHASE, /* <- This action cannot be performed during this phase. */
  LACK_ELEMENT, /* <- You do not own this element type. */
  LACK_RESOURCES, /* <- You need more resources to perform this action. */
  ALREADY_CALLED, /* <- You cannot call this action multiple times. */
} action_error;


///
// Type of elements
//
typedef enum element {
  ELT_FEU, /* <- Feu */
  ELT_EAU, /* <- Eau */
  ELT_TERRE, /* <- Terre */
  ELT_AIR, /* <- Air */
  ELT_LUMIERE, /* <- Lumière */
  ELT_INVALID, /* <- Invalid element */
} element;



#endif // !CONSTANT_HH_
