#include "actions.hh"
#include "dumper.hh"
#include "rules.hh"

Rules::Rules(const rules::Options opt)
  : TurnBasedRules(opt)
  , sandbox_(opt.time)
  , skip_to_player_(-1)
  , skip_to_end_of_round_(false)
{
    if (!opt.champion_lib.empty())
    {
        champion_dll_ = std::make_unique<utils::DLL>(opt.champion_lib);

        champion_game_init_ =
            champion_dll_->get<f_champion_game_init>("game_init");
        champion_bidding_phase_ =
            champion_dll_->get<f_champion_bidding_phase>("bidding_phase");
        champion_outbidding_phase_ =
            champion_dll_->get<f_champion_outbidding_phase>("outbidding_phase");
        champion_fighting_phase_ =
            champion_dll_->get<f_champion_fighting_phase>("fighting_phase");
        champion_game_end_ =
            champion_dll_->get<f_champion_game_end>("game_end");
    }

    GameState* game_state = new GameState(opt.players);
    api_ = std::make_unique<Api>(game_state, opt.player);
    register_actions();
}

void Rules::register_actions()
{
    api_->actions()->register_action(
        ID_ACTION_BID,
        []() -> rules::IAction* { return new ActionBid(); }
        );
    api_->actions()->register_action(
        ID_ACTION_OUTBID,
        []() -> rules::IAction* { return new ActionOutbid(); }
        );
    api_->actions()->register_action(
        ID_ACTION_FIGHT,
        []() -> rules::IAction* { return new ActionFight(); }
        );
}

rules::Actions* Rules::get_actions()
{
    return api_->actions();
}

void Rules::apply_action(const rules::IAction_sptr& action)
{
    int e = action->check(api_->game_state());
    if (!e)
        api_->game_state_set(action->apply(api_->game_state()));
    else
        FATAL("Synchronization error: received action %d from player %d, but "
              "check() on current gamestate returned %d.",
              action->id(), action->player_id(), e);
}

bool Rules::is_finished()
{
    return api_->game_state()->is_finished();
}

void Rules::player_turn()
{
    if (skip_to_end_of_round_)
        return;
    if (skip_to_player_ != -1)
    {
        if (static_cast<unsigned>(skip_to_player_) == api_->player()->id)
            skip_to_player_ = -1;
        else
            return;
    }

    game_phase phase = api_->game_state()->get_phase();
    switch (phase)
    {
        case PHASE_BIDDING:
            sandbox_.execute(champion_bidding_phase_);
            break;
        case PHASE_OUTBIDDING:
            sandbox_.execute(champion_outbidding_phase_);
            break;
        case PHASE_FIGHTING:
            sandbox_.execute(champion_fighting_phase_);
            break;
    }
}

void Rules::at_server_start(rules::ServerMessenger_sptr msgr)
{
    uint32_t seed = static_cast<unsigned int>(
            std::chrono::system_clock::now().time_since_epoch().count());
    api_->game_state()->seed_random(seed);
    msgr->push_id(seed);
}

void Rules::at_player_start(rules::ClientMessenger_sptr msgr)
{
    uint32_t seed;
    msgr->pull_id(&seed);
    api_->game_state()->seed_random(seed);

    sandbox_.execute(champion_game_init_);
}

void Rules::at_player_end(rules::ClientMessenger_sptr)
{
    sandbox_.execute(champion_game_end_);
}

void Rules::start_of_round()
{
    game_phase phase = api_->game_state()->get_phase();
    switch (phase)
    {
        case PHASE_BIDDING:
            api_->game_state()->init_bidding();
            break;
        case PHASE_OUTBIDDING:
            break;
        case PHASE_FIGHTING:
            api_->game_state()->init_fight();
            break;
    }
}

void Rules::end_of_round()
{
    game_phase phase = api_->game_state()->get_phase();
    switch (phase)
    {
        case PHASE_BIDDING:
            skip_to_player_ = api_->game_state()->resolve_bidding();
            break;
        case PHASE_OUTBIDDING:
            if (skip_to_end_of_round_)
                api_->game_state()->resolve_outbidding();
            break;
        case PHASE_FIGHTING:
            api_->game_state()->resolve_fight();
            break;
    }
    skip_to_end_of_round_ = false;
}

void Rules::end_of_player_turn(unsigned player_id)
{
    if (skip_to_end_of_round_)
        return;
    if (skip_to_player_ != -1)
    {
        if (static_cast<unsigned>(skip_to_player_) == player_id)
            skip_to_player_ = -1;
        else
            return;
    }

    game_phase phase = api_->game_state()->get_phase();
    switch (phase)
    {
        case PHASE_BIDDING:
            break;
        case PHASE_FIGHTING:
            api_->game_state()->auto_fight(player_id);
            break;
        case PHASE_OUTBIDDING:
            skip_to_end_of_round_ =
                api_->game_state()->resolve_outbidding_turn(player_id);
            break;
    }
}

void Rules::dump_state(std::ostream& out)
{
    dump_game_state(out, *api_->game_state());
}
