// FIXME License notice

#ifndef GAME_STATE_HH
#define GAME_STATE_HH

#include <chrono>
#include <numeric>
#include <random>
#include <unordered_map>

#include <rules/game-state.hh>
#include <rules/player.hh>

#include "constant.hh"

class ActionTest;

enum game_phase
{
    PHASE_BIDDING,
    PHASE_OUTBIDDING,
    PHASE_FIGHTING,
};

struct player_info
{
    int resources;
    std::array<unsigned, NB_ELEMENTS> elements;

    unsigned initial_bid;
    unsigned current_bid;

    element played_element;
    element last_played_element;

    int *score; // reference to stechec score for convenience
};

class GameState : public rules::GameState
{
    public:
        GameState(rules::Players_sptr players);
        virtual rules::GameState* copy() const;

    private:
        rules::Players_sptr players_;
        unsigned p_[2];

        /* Global state */
        game_phase phase_;
        std::unordered_map<unsigned, player_info> player_info_;

        /* Bidding phase */
        element elt_sold_;
        bool auction_canceled_;
        bool finished_;
        int last_auction_winner_;

        std::mt19937 gen_;

    public:
        game_phase get_phase() const { return phase_; }
        void set_phase(game_phase phase) { phase_ = phase; }

        element get_elt_sold() const { return elt_sold_; }

        const rules::Players_sptr& get_players() const { return players_; }

        unsigned opponent(unsigned player) const
        { return (p_[0] == player) ? p_[1] : p_[0]; }

        int get_score(unsigned player) const
        { return *player_info_.at(player).score; }

        int get_resources(unsigned player) const
        { return player_info_.at(player).resources; }

        unsigned get_initial_bid(unsigned player) const
        { return player_info_.at(player).initial_bid; }

        void set_initial_bid(unsigned player, unsigned amount)
        { player_info_.at(player).initial_bid = amount; }

        unsigned get_current_bid(unsigned player) const
        { return player_info_.at(player).current_bid; }

        void set_current_bid(unsigned player, unsigned amount)
        { player_info_.at(player).current_bid = amount; }

        unsigned get_bid_difference() const
        { return std::abs(static_cast<int>(get_initial_bid(p_[0])) -
                          static_cast<int>(get_initial_bid(p_[1]))); }

        unsigned get_outbid_cost(unsigned player) const
        { return get_bid_difference() + get_current_bid(opponent(player)); }

        void outbid(unsigned player)
        { player_info_.at(player).current_bid = get_outbid_cost(player); }

        unsigned bidding_final_cost() const
        { return std::min(get_current_bid(p_[0]), get_current_bid(p_[1])); }

        bool get_auction_canceled() const
        { return auction_canceled_; }

        int get_last_auction_winner() const
        { return last_auction_winner_; }

        unsigned initial_bid_winner() const
        { return (get_initial_bid(p_[0]) > get_initial_bid(p_[1])) ?
                    p_[0] : p_[1]; }

        unsigned outbid_winner() const
        { return (get_current_bid(p_[0]) > get_current_bid(p_[1])) ?
                    p_[0] : p_[1]; }


        unsigned get_nb_element(unsigned player, element elt) const
        { return player_info_.at(player).elements[elt]; }

        element get_played_element(unsigned player) const
        { return player_info_.at(player).played_element; }

        void set_played_element(unsigned player, element elt)
        { player_info_.at(player).played_element = elt; }

        element get_last_played_element(unsigned player) const
        { return player_info_.at(player).last_played_element; }

        element auction_random_element();

        void init_bidding();
        int resolve_bidding(); // returns winner (= next player)

        void resolve_outbidding();
        bool resolve_outbidding_turn(unsigned last_player_id);

        void init_fight();
        void resolve_fight();
        void auto_fight(unsigned player_id);

        bool is_finished() const;

        void seed_random(long unsigned int seed)
        { gen_.seed(seed); }

        friend class ActionTest;
};

#endif /* !GAME_STATE_HH */

