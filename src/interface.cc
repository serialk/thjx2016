/*
** Stechec project is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** The complete GNU General Public Licence Notice can be found as the
** `NOTICE' file in the root directory.
**
** Copyright (C) 2015 Prologin
*/

#include "api.hh"

#include <iostream>
#include <sstream>
#include <vector>

// from api.cc
extern Api* api;

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& arr)
{
  os << "[";
  typename std::vector<T>::const_iterator it;
  for (it = arr.begin(); it != arr.end(); ++it)
  {
    if (it != arr.begin())
      os << ", ";
    os << *it;
  }
  os << "]";
  return os;
}


// todo avoir un ostringstream a la place de std::string

std::string convert_to_string(int i){
  std::ostringstream s;
  s << i;
  std::string result = s.str();
  return result;
}
std::string convert_to_string(std::string i){
  return i;
}
std::string convert_to_string(bool i){
  return i?"true":"false";
}
std::string convert_to_string(std::vector<int> in){
  if (in.size()){
    std::string s = "[" + convert_to_string(in[0]);
    for (int i = 1, l = in.size(); i < l; i++){
      s = s + ", " + convert_to_string(in[i]);
    }
    return s + "]";
  }else{
    return "[]";
  }
}
std::string convert_to_string(action_error in){
  switch (in)
  {
    case OK: return "\"ok\"";
    case INVALID_ARGUMENT: return "\"invalid_argument\"";
    case WRONG_PHASE: return "\"wrong_phase\"";
    case LACK_ELEMENT: return "\"lack_element\"";
    case LACK_RESOURCES: return "\"lack_resources\"";
    case ALREADY_CALLED: return "\"already_called\"";
  }
  return "bad value";
}
std::string convert_to_string(std::vector<action_error> in){
  if (in.size()){
    std::string s = "[" + convert_to_string(in[0]);
    for (int i = 1, l = in.size(); i < l; i++){
      s = s + ", " + convert_to_string(in[i]);
    }
    return s + "]";
  }else{
    return "[]";
  }
}
std::string convert_to_string(element in){
  switch (in)
  {
    case ELT_FEU: return "\"elt_feu\"";
    case ELT_EAU: return "\"elt_eau\"";
    case ELT_TERRE: return "\"elt_terre\"";
    case ELT_AIR: return "\"elt_air\"";
    case ELT_LUMIERE: return "\"elt_lumiere\"";
    case ELT_INVALID: return "\"elt_invalid\"";
  }
  return "bad value";
}
std::string convert_to_string(std::vector<element> in){
  if (in.size()){
    std::string s = "[" + convert_to_string(in[0]);
    for (int i = 1, l = in.size(); i < l; i++){
      s = s + ", " + convert_to_string(in[i]);
    }
    return s + "]";
  }else{
    return "[]";
  }
}
///
// Bid resources in the elements auction.
//
extern "C" action_error api_bid(int amount)
{
  return api->bid(amount);
}

///
// Outbid the current best bid.
//
extern "C" action_error api_outbid()
{
  return api->outbid();
}

///
// Play an element against the other player.
//
extern "C" action_error api_fight(element elt)
{
  return api->fight(elt);
}

///
// Returns your player identifier (not always in {0, 1}).
//
extern "C" int api_me()
{
  return api->me();
}

///
// Returns your opponent identifier (not always in {0, 1}).
//
extern "C" int api_opponent()
{
  return api->opponent();
}

///
// The current score of the specified player.
//
extern "C" int api_score(int player)
{
  return api->score(player);
}

///
// The set of elements owned by a specific player.
//
extern "C" std::vector<element> api_elements_owned(int player)
{
  return api->elements_owned(player);
}

///
// The quantity of an element owned by a specific player.
//
extern "C" int api_nb_element_owned(int player, element elt)
{
  return api->nb_element_owned(player, elt);
}

///
// The quantity of all the elements owned by a specific player.
//
extern "C" int api_nb_elements_owned(int player)
{
  return api->nb_elements_owned(player);
}

///
// The gain of an element played against another (returns a value from the gain matrix).
//
extern "C" int api_resources_gain(element yours, element theirs)
{
  return api->resources_gain(yours, theirs);
}

///
// The amount of resources owned by a player.
//
extern "C" int api_resources_owned(int player)
{
  return api->resources_owned(player);
}

///
// The last element that was played during a fight by the specified player. Returns ``elt_invalid`` when called during the first turn.
//
extern "C" element api_last_element_played(int player)
{
  return api->last_element_played(player);
}

///
// The element currently sold in auction. Returns ``elt_invalid`` if not in auction phase.
//
extern "C" element api_element_sold()
{
  return api->element_sold();
}

///
// The initial bid of a specific player. Returns -1 if not in auction phase.
//
extern "C" int api_player_initial_bid(int player)
{
  return api->player_initial_bid(player);
}

///
// The current bid of a specific player. Returns -1 if not in auction phase.
//
extern "C" int api_player_bid(int player)
{
  return api->player_bid(player);
}

///
// The total cost of outbidding ($m_k$). Returns -1 if not in outbidding phase.
//
extern "C" int api_outbid_cost()
{
  return api->outbid_cost();
}

///
// The difference between the initial bids. Returns -1 if not in outbidding phase.
//
extern "C" int api_bid_difference()
{
  return api->bid_difference();
}

///
// Returns ``true`` if the last auction was canceled and ``false`` in all the other cases.
//
extern "C" bool api_auction_canceled()
{
  return api->auction_canceled();
}

///
// The identifier of the last player who won the auction. Returns -1 if the auction was canceled.
//
extern "C" int api_auction_winner()
{
  return api->auction_winner();
}

///
// Affiche le contenu d'une valeur de type action_error
//
std::ostream& operator<<(std::ostream& os, action_error v)
{
  switch (v) {
  case OK: os << "OK"; break;
  case INVALID_ARGUMENT: os << "INVALID_ARGUMENT"; break;
  case WRONG_PHASE: os << "WRONG_PHASE"; break;
  case LACK_ELEMENT: os << "LACK_ELEMENT"; break;
  case LACK_RESOURCES: os << "LACK_RESOURCES"; break;
  case ALREADY_CALLED: os << "ALREADY_CALLED"; break;
  }
  return os;
}
extern "C" void api_afficher_action_error(action_error v)
{
  std::cerr << v << std::endl;
}

///
// Affiche le contenu d'une valeur de type element
//
std::ostream& operator<<(std::ostream& os, element v)
{
  switch (v) {
  case ELT_FEU: os << "ELT_FEU"; break;
  case ELT_EAU: os << "ELT_EAU"; break;
  case ELT_TERRE: os << "ELT_TERRE"; break;
  case ELT_AIR: os << "ELT_AIR"; break;
  case ELT_LUMIERE: os << "ELT_LUMIERE"; break;
  case ELT_INVALID: os << "ELT_INVALID"; break;
  }
  return os;
}
extern "C" void api_afficher_element(element v)
{
  std::cerr << v << std::endl;
}

