/*
** Stechec project is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** The complete GNU General Public Licence Notice can be found as the
** `NOTICE' file in the root directory.
**
** Copyright (C) 2015 Prologin
*/

#include <stdlib.h>

#include "api.hh"
#include "actions.hh"
#include "constant.hh"
#include "table_moves.hh"

// global used in interface.cc
Api* api;

Api::Api(GameState* game_state, rules::Player_sptr player)
    : game_state_(game_state),
      player_(player)
{
    api = this;
}


///
// Bid resources in the elements auction.
//
action_error Api::bid(int amount)
{
    rules::IAction_sptr action(new ActionBid(amount, player_->id));

    action_error err;
    if ((err = static_cast<action_error>(action->check(game_state_))) != OK)
        return err;

    actions_.add(action);
    game_state_set(action->apply(game_state_));
    return OK;
}

///
// Outbid the current best bid.
//
action_error Api::outbid()
{
    rules::IAction_sptr action(new ActionOutbid(player_->id));

    action_error err;
    if ((err = static_cast<action_error>(action->check(game_state_))) != OK)
        return err;

    actions_.add(action);
    game_state_set(action->apply(game_state_));
    return OK;
}

///
// Play an element against the other player.
//
action_error Api::fight(element elt)
{
    rules::IAction_sptr action(new ActionFight(elt, player_->id));

    action_error err;
    if ((err = static_cast<action_error>(action->check(game_state_))) != OK)
        return err;

    actions_.add(action);
    game_state_set(action->apply(game_state_));
    return OK;
}

///
// Returns your player identifier (not always in {0, 1}).
//
int Api::me()
{
    return player_->id;
}

///
// Returns your opponent identifier (not always in {0, 1}).
//
int Api::opponent()
{
    return game_state()->opponent(me());
}

///
// The current score of the specified player.
//
int Api::score(int player)
{
    return game_state()->get_score(player);
}

///
// The set of elements owned by a specific player.
//
std::vector<element> Api::elements_owned(int player)
{
    std::vector<element> res;
    for (unsigned e = 0; e < NB_ELEMENTS; ++e)
    {
        element elt = static_cast<element>(e);
        if (game_state()->get_nb_element(player, elt) > 0)
            res.push_back(elt);
    }
    return res;
}

///
// The quantity of an element owned by a specific player.
//
int Api::nb_element_owned(int player, element elt)
{
    return game_state()->get_nb_element(player, elt);
}

///
// The quantity of all the elements owned by a specific player.
//
int Api::nb_elements_owned(int player)
{
    unsigned res = 0;
    for (unsigned e = 0; e < NB_ELEMENTS; ++e)
    {
        element elt = static_cast<element>(e);
        res += game_state()->get_nb_element(player, elt);
    }
    return res;
}

///
// The gain of an element played against another (returns a value from the gain matrix).
//
int Api::resources_gain(element yours, element theirs)
{
    return table_moves[yours][theirs];
}

///
// The amount of resources owned by a player.
//
int Api::resources_owned(int player)
{
    return game_state()->get_resources(player);
}

///
// The last element that was played during a fight by the specified player. Returns ``elt_invalid`` when called during the first turn.
//
element Api::last_element_played(int player)
{
    return game_state()->get_last_played_element(player);
}

///
// The element currently sold in auction. Returns ``elt_invalid`` if not in auction phase.
//
element Api::element_sold()
{
    game_phase phase = game_state()->get_phase();
    if (phase != PHASE_BIDDING && phase != PHASE_OUTBIDDING)
        return ELT_INVALID;
    return game_state()->get_elt_sold();
}

///
// The initial bid of a specific player. Returns -1 if not in auction phase.
//
int Api::player_initial_bid(int player)
{
    game_phase phase = game_state()->get_phase();
    if (phase != PHASE_OUTBIDDING)
        return -1;
    return game_state()->get_initial_bid(player);
}

///
// The current bid of a specific player. Returns -1 if not in auction phase.
//
int Api::player_bid(int player)
{
    game_phase phase = game_state()->get_phase();
    if (phase != PHASE_OUTBIDDING)
        return -1;
    return game_state()->get_current_bid(player);
}

///
// The total cost of outbidding ($m_k$). Returns -1 if not in outbidding phase.
//
int Api::outbid_cost()
{
    game_phase phase = game_state()->get_phase();
    if (phase != PHASE_OUTBIDDING)
        return -1;
    return game_state()->get_outbid_cost(me());
}

///
// The difference between the initial bids. Returns -1 if not in outbidding phase.
//
int Api::bid_difference()
{
    game_phase phase = game_state()->get_phase();
    if (phase != PHASE_OUTBIDDING)
        return -1;
    return game_state()->get_bid_difference();
}

///
// Returns ``true`` if the last auction was canceled and ``false`` in all the other cases.
//
bool Api::auction_canceled()
{
    game_phase phase = game_state()->get_phase();
    return (phase == PHASE_FIGHTING && game_state()->get_auction_canceled());
}

///
// The identifier of the last player who won the auction. Returns -1 if the auction was canceled.
//
int Api::auction_winner()
{
    return game_state()->get_last_auction_winner();
}

///
// Affiche le contenu d'une valeur de type action_error
//

///
// Affiche le contenu d'une valeur de type element
//


