/*
** Stechec project is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** The complete GNU General Public Licence Notice can be found as the
** `NOTICE' file in the root directory.
**
** Copyright (C) 2015 Prologin
*/

#ifndef API_HH_
#define API_HH_

#include <vector>
#include <rules/game-state.hh>
#include <rules/player.hh>
#include <rules/actions.hh>

#include "game_state.hh"
#include "constant.hh"

/*!
** The methods of this class are exported through 'interface.cc'
** to be called by the clients
*/
class Api
{

public:
    Api(GameState* game_state, rules::Player_sptr player);
    virtual ~Api() { }

    const rules::Player_sptr player() const { return player_; }
    void player_set(rules::Player_sptr player) { player_ = player; }

    rules::Actions* actions() { return &actions_; }

    const GameState* game_state() const { return game_state_; }
    GameState* game_state() { return game_state_; }
    void game_state_set(rules::GameState* gs) {
        game_state_ = dynamic_cast<GameState*>(gs);
    }

private:
    GameState* game_state_;
    rules::Player_sptr player_;
    rules::Actions actions_;

public:

///
// Bid resources in the elements auction.
//
   action_error bid(int amount);
///
// Outbid the current best bid.
//
   action_error outbid();
///
// Play an element against the other player.
//
   action_error fight(element elt);
///
// Returns your player identifier (not always in {0, 1}).
//
   int me();
///
// Returns your opponent identifier (not always in {0, 1}).
//
   int opponent();
///
// The current score of the specified player.
//
   int score(int player);
///
// The set of elements owned by a specific player.
//
   std::vector<element> elements_owned(int player);
///
// The quantity of an element owned by a specific player.
//
   int nb_element_owned(int player, element elt);
///
// The quantity of all the elements owned by a specific player.
//
   int nb_elements_owned(int player);
///
// The gain of an element played against another (returns a value from the gain matrix).
//
   int resources_gain(element yours, element theirs);
///
// The amount of resources owned by a player.
//
   int resources_owned(int player);
///
// The last element that was played during a fight by the specified player. Returns ``elt_invalid`` when called during the first turn.
//
   element last_element_played(int player);
///
// The element currently sold in auction. Returns ``elt_invalid`` if not in auction phase.
//
   element element_sold();
///
// The initial bid of a specific player. Returns -1 if not in auction phase.
//
   int player_initial_bid(int player);
///
// The current bid of a specific player. Returns -1 if not in auction phase.
//
   int player_bid(int player);
///
// The total cost of outbidding ($m_k$). Returns -1 if not in outbidding phase.
//
   int outbid_cost();
///
// The difference between the initial bids. Returns -1 if not in outbidding phase.
//
   int bid_difference();
///
// Returns ``true`` if the last auction was canceled and ``false`` in all the other cases.
//
   bool auction_canceled();
///
// The identifier of the last player who won the auction. Returns -1 if the auction was canceled.
//
   int auction_winner();
///
// Affiche le contenu d'une valeur de type action_error
//

///
// Affiche le contenu d'une valeur de type element
//

};


#endif /* !API_HH_ */
