#ifndef ACTION_FIGHT_HH
#define ACTION_FIGHT_HH

#include <rules/action.hh>

#include "actions.hh"
#include "game_state.hh"
#include "constant.hh"

class ActionFight : public rules::Action<GameState>
{
public:
    ActionFight(element elt, int player_id) : elt_(elt), player_id_(player_id) {}
    ActionFight() {} // for register_action()

    virtual int check(const GameState* st) const;
    virtual void apply_on(GameState* st) const;

    virtual void handle_buffer(utils::Buffer& buf)
    {
        buf.handle(elt_);
        buf.handle(player_id_);
    }

    uint32_t player_id() const { return player_id_; };
    uint32_t id() const { return ID_ACTION_FIGHT; }

private:
    element elt_;
    int player_id_;
};

#endif // !ACTION_FIGHT_HH
