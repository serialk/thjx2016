#include "../action_bid.hh"
#include "test-helpers.hh"

// All the tests below are methods coming from the ActionTest class (see
// test-helpers.hh).  This is where the "st" GameState is coming from.

// Test that player has sufficient resources for the bid
TEST_F(ActionTest, ActionOutbid_enough_resources)
{
    setup_outbid();

    EXPECT_EQ(10u, st->get_bid_difference());
    EXPECT_EQ(30u, st->get_outbid_cost(PLAYER_1));
    {
        ActionOutbid action(PLAYER_1);
        EXPECT_EQ(OK, action.check(st));
        action.apply_on(st);
    }

    EXPECT_EQ(20u, st->bidding_final_cost());

    {
        ActionOutbid action(PLAYER_2);
        EXPECT_EQ(40u, st->get_outbid_cost(PLAYER_2));
        EXPECT_EQ(LACK_RESOURCES, action.check(st));
        get_player_info(PLAYER_2).resources = 40;
        EXPECT_EQ(OK, action.check(st));
        action.apply_on(st);
    }
    EXPECT_EQ(30u, st->bidding_final_cost());
}

// Test that we are in the correct phase
TEST_F(ActionTest, ActionOutbid_correct_phase)
{
    get_player_info(PLAYER_1).resources = 30;
    ActionOutbid action(PLAYER_1);

    get_game_phase() = PHASE_OUTBIDDING;
    EXPECT_EQ(OK, action.check(st));

    get_game_phase() = PHASE_FIGHTING;
    EXPECT_EQ(WRONG_PHASE, action.check(st));

    get_game_phase() = PHASE_BIDDING;
    EXPECT_EQ(WRONG_PHASE, action.check(st));
}

// Test wrong player
TEST_F(ActionTest, ActionOutbid_wrong_player)
{
    setup_outbid();

    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).resources = 100;
        ActionOutbid action(player);
        EXPECT_EQ(OK, action.check(st));
        action.apply_on(st);
    }
    for (int player = 2; player < 5; ++player)
    {
        ActionOutbid action(player);
        try
        {
            EXPECT_EQ(INVALID_ARGUMENT, action.check(st));
        }
        catch(...)
        {
          // OK
        }
    }
}

// Test repeat_bid
TEST_F(ActionTest, ActionOutbid_repeat)
{
    setup_outbid();

    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).resources = 100;
        ActionOutbid action(player);
        EXPECT_EQ(OK, action.check(st));
        action.apply_on(st);
        EXPECT_EQ(ALREADY_CALLED, action.check(st));
    }
}
