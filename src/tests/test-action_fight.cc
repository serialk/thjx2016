#include "../action_bid.hh"
#include "test-helpers.hh"

// All the tests below are methods coming from the ActionTest class (see
// test-helpers.hh).  This is where the "st" GameState is coming from.

// Test that player has sufficient resources for the bid
TEST_F(ActionTest, ActionFight_enough_element)
{
    get_game_phase() = PHASE_FIGHTING;
    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).elements[ELT_FEU] = 0;
        ActionFight action(ELT_FEU, player);

        EXPECT_EQ(LACK_ELEMENT, action.check(st));

        get_player_info(player).elements[ELT_FEU] = 30;
        EXPECT_EQ(OK, action.check(st));
    }
}

// Test that we are in the correct phase
TEST_F(ActionTest, ActionFight_correct_phase)
{
    get_player_info(PLAYER_1).elements[ELT_FEU] = 30;
    ActionFight action(ELT_FEU, PLAYER_1);

    get_game_phase() = PHASE_OUTBIDDING;
    EXPECT_EQ(WRONG_PHASE, action.check(st));

    get_game_phase() = PHASE_FIGHTING;
    EXPECT_NE(WRONG_PHASE, action.check(st));

    get_game_phase() = PHASE_BIDDING;
    EXPECT_EQ(WRONG_PHASE, action.check(st));
}

// Test wrong player
TEST_F(ActionTest, ActionFight_wrong_player)
{
    get_game_phase() = PHASE_FIGHTING;
    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).elements[ELT_FEU] = 10;
        ActionFight action(ELT_FEU, player);
        EXPECT_EQ(OK, action.check(st));
    }
    for (int player = 2; player < 5; ++player)
    {
        ActionFight action(ELT_FEU, player);
        try
        {
            EXPECT_EQ(INVALID_ARGUMENT, action.check(st));
        }
        catch(...)
        {
          // OK
        }
    }
}

// Test repeat_bid
TEST_F(ActionTest, ActionFight_repeat)
{
    get_game_phase() = PHASE_FIGHTING;
    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).elements[ELT_FEU] = 30;
        ActionFight action(ELT_FEU, player);
        EXPECT_EQ(OK, action.check(st));
        action.apply_on(st);
        EXPECT_EQ(ALREADY_CALLED, action.check(st));
    }
}

// Test all elements
TEST_F(ActionTest, ActionFight_all_elements)
{
    get_game_phase() = PHASE_FIGHTING;
    for (int player : {PLAYER_1, PLAYER_2})
    {
        for (element el : {ELT_FEU, ELT_EAU, ELT_AIR, ELT_TERRE, ELT_LUMIERE})
        {
            get_player_info(player).elements[el] = 30;
            ActionFight action(el, player);
            EXPECT_EQ(OK, action.check(st));
        }
    }
}
