#include "../action_bid.hh"
#include "test-helpers.hh"

#include <array>

#define APPROX_FACTOR 150

// All the tests below are methods coming from the ActionTest class (see
// test-helpers.hh).  This is where the "st" GameState is coming from.

// Test element distribution, all equiprobable
TEST_F(ActionTest, InitBid_uniform)
{
    std::array<int, NB_ELEMENTS> hist{{}};
    for (int i = 0; i < 10000; ++i)
    {
        st->init_bidding();
        EXPECT_NE(ELT_INVALID, st->get_elt_sold());
        ++hist[st->get_elt_sold()];
    }
    for (int i = 1; i < NB_ELEMENTS; ++i)
        EXPECT_EQ(0, abs(hist[i-1] - hist[i]) / APPROX_FACTOR);
}

// Test element distribution, weighted
TEST_F(ActionTest, InitBid_weighted)
{
    std::array<int, NB_ELEMENTS> weights{{1, 2, 3, 4, 5}};
    for(auto p : {PLAYER_1, PLAYER_2})
        for (auto el : {ELT_FEU, ELT_EAU, ELT_TERRE, ELT_AIR, ELT_LUMIERE})
            get_player_info(p).elements[el] = weights[el];

    std::array<int, NB_ELEMENTS> hist{{}};
    for (int i = 0; i < 10000; ++i)
    {
        st->init_bidding();
        EXPECT_NE(ELT_INVALID, st->get_elt_sold());
        ++hist[st->get_elt_sold()];
    }
    for (int i = 1; i < NB_ELEMENTS; ++i)
        EXPECT_EQ(0, abs(hist[i-1] / weights[i-1] - hist[i] / weights[i]) / APPROX_FACTOR);
}

// Test player setup
TEST_F(ActionTest, InitBid_setud)
{
    st->init_bidding();
    EXPECT_EQ(PHASE_BIDDING, st->get_phase());
    EXPECT_NE(ELT_INVALID, st->get_elt_sold());
    EXPECT_EQ(0u, st->get_bid_difference());
    EXPECT_EQ(0u, st->bidding_final_cost());
    ASSERT_FALSE(st->get_auction_canceled());
    EXPECT_EQ(-1, st->get_last_auction_winner());

    for(auto p : {PLAYER_1, PLAYER_2})
    {
        EXPECT_EQ(0, st->get_score(p));
        EXPECT_EQ(1000, st->get_resources(p));
        EXPECT_EQ(0u, st->get_initial_bid(p));
        EXPECT_EQ(0u, st->get_current_bid(p));
        EXPECT_EQ(0u, st->get_outbid_cost(p));
        EXPECT_EQ(ELT_INVALID, st->get_played_element(p));
        EXPECT_EQ(ELT_INVALID, st->get_last_played_element(p));
        for (auto el : {ELT_FEU, ELT_EAU, ELT_TERRE, ELT_AIR, ELT_LUMIERE})
            EXPECT_EQ(1u, st->get_nb_element(p, el));
    }

    ASSERT_FALSE(st->is_finished());
}
