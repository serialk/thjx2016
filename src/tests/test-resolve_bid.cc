#include "../action_bid.hh"
#include "test-helpers.hh"

// All the tests below are methods coming from the ActionTest class (see
// test-helpers.hh).  This is where the "st" GameState is coming from.

// Test cancelled auction
TEST_F(ActionTest, ActionBid_cancelled)
{
    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).resources = 30;
        ActionBid action(10, player);
        EXPECT_EQ(OK, action.check(st));
        action.apply_on(st);
    }
    EXPECT_EQ(-1, st->resolve_bidding());
    EXPECT_EQ(true, st->get_auction_canceled());
    EXPECT_EQ(-1, st->get_last_auction_winner());
    EXPECT_EQ(PHASE_FIGHTING, get_game_phase());
}

// Test not cancelled auction
TEST_F(ActionTest, ActionBid_not_cancelled)
{
    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).resources = 30;
        ActionBid action(10 + player, player);
        EXPECT_EQ(OK, action.check(st));
        action.apply_on(st);
    }
    EXPECT_EQ(static_cast<int>(PLAYER_2), st->resolve_bidding());
    EXPECT_EQ(PLAYER_2, st->initial_bid_winner());
    ASSERT_FALSE(st->get_auction_canceled());
    EXPECT_EQ(-1, st->get_last_auction_winner());
    EXPECT_EQ(PHASE_OUTBIDDING, get_game_phase());
}
