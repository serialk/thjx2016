#ifndef TEST_HELPERS_HH
#define TEST_HELPERS_HH

#include <sstream>

#include <gtest/gtest.h>

#include "../constant.hh"
#include "../game_state.hh"
#include "../action_bid.hh"



class ActionTest : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
        utils::Logger::get().level() = utils::Logger::DEBUG_LEVEL;

        /* Create two players (no spectator).  */
        rules::Players_sptr players(new rules::Players {
            std::vector<rules::Player_sptr> {
                rules::Player_sptr(new rules::Player(0, rules::PLAYER)),
                rules::Player_sptr(new rules::Player(1, rules::PLAYER)),
            }
        });

        st = new GameState(players);
    }

    virtual void TearDown()
    {
        delete st;
    }

    GameState* st;

    const unsigned int PLAYER_1 = 0;
    const unsigned int PLAYER_2 = 1;

    player_info& get_player_info(unsigned player)
    { return st->player_info_.at(player); }

    game_phase& get_game_phase()
    { return st->phase_; }

    void setup_outbid()
    {
        get_game_phase() = PHASE_BIDDING;
        for (int player : {PLAYER_1, PLAYER_2})
        {
            get_player_info(player).resources = 30;
            ActionBid action(10 * player + 10, player);
            EXPECT_EQ(OK, action.check(st));
            action.apply_on(st);
        }
        get_game_phase() = PHASE_OUTBIDDING;
    }
};

#endif /* !TESTHELPERS_HH_ */
