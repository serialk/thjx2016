#include "../action_bid.hh"
#include "test-helpers.hh"

// All the tests below are methods coming from the ActionTest class (see
// test-helpers.hh).  This is where the "st" GameState is coming from.

// Test that player has sufficient resources for the bid
TEST_F(ActionTest, ActionBid_enough_resources)
{
    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).resources = 30;
        ActionBid action(40, player);

        EXPECT_EQ(LACK_RESOURCES, action.check(st));

        get_player_info(player).resources = 50;
        EXPECT_EQ(OK, action.check(st));
    }
}

// Test that we are in the correct phase
TEST_F(ActionTest, ActionBid_correct_phase)
{
    get_player_info(PLAYER_1).resources = 30;
    ActionBid action(1, PLAYER_1);

    get_game_phase() = PHASE_OUTBIDDING;
    EXPECT_EQ(WRONG_PHASE, action.check(st));

    get_game_phase() = PHASE_FIGHTING;
    EXPECT_EQ(WRONG_PHASE, action.check(st));

    get_game_phase() = PHASE_BIDDING;
    EXPECT_EQ(OK, action.check(st));
}

// Test that amount is positive
TEST_F(ActionTest, ActionBid_amount_positive)
{
    for (int player : {PLAYER_1, PLAYER_2})
    {
        ActionBid action(-30, player);
        EXPECT_EQ(INVALID_ARGUMENT, action.check(st));
    }
}

// Test wrong player
TEST_F(ActionTest, ActionBid_wrong_player)
{
    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).resources = 30;
        ActionBid action(1, player);
        EXPECT_EQ(OK, action.check(st));
    }
    for (int player = 2; player < 5; ++player)
    {
        ActionBid action(1, player);
        try
        {
            EXPECT_EQ(INVALID_ARGUMENT, action.check(st));
        }
        catch(...)
        {
          // OK
        }
    }
}

// Test repeat_bid
TEST_F(ActionTest, ActionBid_repeat)
{
    for (int player : {PLAYER_1, PLAYER_2})
    {
        get_player_info(player).resources = 30;
        ActionBid action(10, player);
        EXPECT_EQ(OK, action.check(st));
        action.apply_on(st);
        EXPECT_EQ(ALREADY_CALLED, action.check(st));
    }
}
