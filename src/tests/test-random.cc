#include "test-helpers.hh"

// All the tests below are methods coming from the ActionTest class (see
// test-helpers.hh).  This is where the "st" GameState is coming from.

// Test element distribution, all equiprobable
TEST_F(ActionTest, seed_random)
{
    rules::Players_sptr players(new rules::Players {
                                    std::vector<rules::Player_sptr> {
                                        rules::Player_sptr(new rules::Player(0, rules::PLAYER)),
                                        rules::Player_sptr(new rules::Player(1, rules::PLAYER)),
                                    }
                                });
    GameState g1{players};
    g1.seed_random(424071337);
    GameState g2{players};
    g2.seed_random(424071337);
    for (int i = 0; i < 1000; ++i)
        EXPECT_EQ(g1.auction_random_element(), g2.auction_random_element());
}
