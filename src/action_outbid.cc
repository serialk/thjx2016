#include "actions.hh"

int ActionOutbid::check(const GameState* st) const
{
    if (st->get_phase() != PHASE_OUTBIDDING)
        return WRONG_PHASE;

    if (st->get_current_bid(player_id_) >
        st->get_current_bid(st->opponent(player_id_)))
        return ALREADY_CALLED;

    if (static_cast<int>(st->get_outbid_cost(player_id_)) >
            st->get_resources(player_id_))
        return LACK_RESOURCES;

    return OK;
}

void ActionOutbid::apply_on(GameState* st) const
{
    st->outbid(player_id_);
}
