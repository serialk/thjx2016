#include "actions.hh"

int ActionFight::check(const GameState* st) const
{
    if (st->get_phase() != PHASE_FIGHTING)
        return WRONG_PHASE;

    if (st->get_played_element(player_id_) != ELT_INVALID)
        return ALREADY_CALLED;

    if (elt_ != ELT_FEU && elt_ != ELT_EAU && elt_ != ELT_TERRE &&
        elt_ != ELT_AIR && elt_ != ELT_LUMIERE)
        return INVALID_ARGUMENT;

    if (st->get_nb_element(player_id_, elt_) == 0)
        return LACK_ELEMENT;

    return OK;
}

void ActionFight::apply_on(GameState* st) const
{
    st->set_played_element(player_id_, elt_);
}
