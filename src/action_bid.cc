#include "actions.hh"

int ActionBid::check(const GameState* st) const
{
    if (st->get_phase() != PHASE_BIDDING)
        return WRONG_PHASE;

    if (st->get_initial_bid(player_id_) > 0)
        return ALREADY_CALLED;

    if (amount_ < 0)
        return INVALID_ARGUMENT;

    if (amount_ > st->get_resources(player_id_))
        return LACK_RESOURCES;

    return OK;
}

void ActionBid::apply_on(GameState* st) const
{
    st->set_initial_bid(player_id_, amount_);
    st->set_current_bid(player_id_, amount_);
}
