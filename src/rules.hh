#ifndef RULES_RULES_HH_
#define RULES_RULES_HH_

#include <memory>

#include <utils/dll.hh>
#include <utils/sandbox.hh>
#include <rules/options.hh>
#include <rules/client-messenger.hh>
#include <rules/server-messenger.hh>
#include <rules/player.hh>
#include <rules/rules.hh>
#include <rules/actions.hh>

#include "api.hh"

typedef void (*f_champion_game_init)();
typedef void (*f_champion_bidding_phase)();
typedef void (*f_champion_outbidding_phase)();
typedef void (*f_champion_fighting_phase)();
typedef void (*f_champion_game_end)();

class Rules : public rules::TurnBasedRules
{
public:
    explicit Rules(const rules::Options opt);
    virtual ~Rules()
    {}

    virtual rules::Actions* get_actions();
    virtual void apply_action(const rules::IAction_sptr& action);
    virtual bool is_finished();

    void at_server_start(rules::ServerMessenger_sptr msgr);
    void at_player_start(rules::ClientMessenger_sptr msgr);
    void at_player_end(rules::ClientMessenger_sptr msgr);

    void player_turn();

    void start_of_round();
    void end_of_round();

    void end_of_player_turn(unsigned player_id);

    void dump_state(std::ostream& out);

protected:
    f_champion_game_init champion_game_init_;
    f_champion_bidding_phase champion_bidding_phase_;
    f_champion_outbidding_phase champion_outbidding_phase_;
    f_champion_fighting_phase champion_fighting_phase_;
    f_champion_game_end champion_game_end_;

private:
    void register_actions();

    std::unique_ptr<utils::DLL> champion_dll_;
    std::unique_ptr<Api> api_;
    utils::Sandbox sandbox_;

    int skip_to_player_;
    int skip_to_end_of_round_;
};

#endif // !RULES_RULES_HH_
