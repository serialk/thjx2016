$(document).ready(function()
{
    $("#replay").css('visibility', 'hidden');

    $.get('/static/replay/replay_content.html', {},
            function (data) {
                $("#replay").html(data);
            });

    var s = document.createElement("script");
    s.type = "text/python";
    s.src = "/static/replay/replay.py";
    $("head").append(s);

//    $.getScript("/static/js/brython.js",
//            function(data, textStatus, jqxhr) {
//                brython({"debug": 1, "pythonpath": ["static/js", 'djfhs']});
//            });
});
