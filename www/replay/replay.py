from browser import document as doc, window as win
from browser import ajax, html
import time
import json
from javascript import JSConstructor
Slider = JSConstructor(win.Slider)

def post(url, data, callback, timeout=1):
    req = ajax.ajax()
    req.bind('complete', callback)
    req.open('POST', url, True)
    req.set_header('content-type','application/x-www-form-urlencoded')
    req.send(data)

def get(url, callback, timeout=1):
    req = ajax.ajax()
    req.bind('complete', callback)
    req.open('GET', url, True)
    req.send()

def exportjs(func):
    setattr(win, func.__name__, func)
    return func

actions = None
turn_time = 1000
turn = 0
timeout_handle = None
play = True
elt_map = {0: "fire", 1: "water", 2: "earth", 3: "air", 4: "light"}
bid_html = win.phase_bid.html
fight_html = win.phase_fight.html
last_bid = None

slider = Slider(win.turn_slider)

def set_player(n, name):
    getattr(win, "player" + str(n)).text = name

def set_player_resources(n, resources):
    getattr(win, "score" + str(n)).text = resources["score"]
    getattr(win, "money" + str(n)).text = resources["resources"]
    for elt, nb in resources["elements"].items():
        getattr(win, elt_map[int(elt)] + n).text = nb

def set_slider(turn, total=None):
    if total is not None:
        slider.setAttribute("max", total)
    slider.setValue(turn)

def item_img(item):
    if item < 0 or item > 4:
        # FIXME
        item = 0
    return "/static/img/icons/{}.svg".format(elt_map[item])

@exportjs
def toggle_play(value=None):
    global play
    play = (not play) if value is None else value
    if play:
        replay_action()
        win.play_icon.Class = "glyphicon glyphicon-pause"
    else:
        win.clearTimeout(timeout_handle)
        win.play_icon.Class = "glyphicon glyphicon-play"

@exportjs
def add_turn(offset):
    val = slider.getValue() + offset
    if val >= 0 and val < len(actions):
        slider.setValue(val)
        change_turn()

def change_turn(useless_event=None):
    global turn
    toggle_play(False)
    turn = slider.getValue()
    replay_action()

def display_bid(bid1, bid2, elt):
    global last_bid
    win.phase.text = "Enchères"
    win.panel.html = bid_html
    if last_bid is not None:
        win.old_bid1.text = last_bid[0]
        win.old_bid2.text = last_bid[1]
    win.bid1.text = str(bid1)
    win.bid2.text = str(bid2)
    last_bid = str(bid1), str(bid2)
    win.bid_elt.src = item_img(elt)
    if bid1 > bid2:
        win.bid1.Class = "text-success"
        win.bid2.Class = "text-danger"
    elif bid1 < bid2:
        win.bid2.Class = "text-success"
        win.bid1.Class = "text-danger"
    else:
        win.bid1.Class = "text-warning"
        win.bid2.Class = "text-warning"

def display_fight(elt1, elt2):
    global last_bid
    win.phase.text = "Combat"
    win.panel.html = fight_html
    win.fight_elt1.src = item_img(elt1)
    win.fight_elt2.src = item_img(elt2)
    last_bid = None

def replay_action():
    try:
        action = actions[turn]
        set_slider(turn)
        for n in action["players"]:
            set_player_resources(n, action["players"][n])
        if "BID" in action["phase"]:
            display_bid(int(action["players"]["1"]["current_bid"]),
                        int(action["players"]["2"]["current_bid"]),
                        int(action["element_sold"]))
        else:
            display_fight(int(action["players"]["1"]["played_element"]),
                          int(action["players"]["2"]["played_element"]))
        if (play):
            turn += 1
            timeout_handle = win.setTimeout(replay_action, turn_time)
    except StopIteration:
        return

def replay_json(req):
    global actions
    actions = [json.loads(a) for a in req.text.split('\n') if a != ""]

    for i, p in enumerate(actions[0]['players'].values(), 1):
        set_player(i, p['name'])

    win.replay.style.visibility = "visible"

    # Remove initial state
    actions = actions[1:]

    # Remove redundant fights and bidding
    new_actions = []
    fight_passed = False
    bidding_passed = False
    last_bid = (-1, -1)
    for i in range(len(actions)):
        if 'PHASE_BIDDING' in actions[i]['phase']:
            last_bid = (int(actions[i]['players']['1']['current_bid']),
                        int(actions[i]['players']['2']['current_bid']))

            if bidding_passed:
                bidding_passed = False
            else:
                bidding_passed = True
                continue
        elif 'PHASE_OUTBIDDING' in actions[i]['phase']:
            bid = (int(actions[i]['players']['1']['current_bid']),
                   int(actions[i]['players']['2']['current_bid']))
            if bid == last_bid:
                continue
            last_bid = bid
        elif 'PHASE_FIGHTING' in actions[i]['phase']:
            last_bid = (-1, -1)
            if fight_passed:
                fight_passed = False
            else:
                fight_passed = True
                continue
        new_actions.append(actions[i])
    actions = new_actions

    set_slider(0, len(actions) - 1)
    replay_action()

def replay():
    slider.on("change", change_turn)
    win.slider.style.width = "100%"
    doc.get(selector=".slider-selection")[0].style.background = "#449D44"
    get("dump/", replay_json)

replay()
